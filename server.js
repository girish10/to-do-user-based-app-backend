const express = require("express");
const User =  require("./Model/UserSchema");
const TodoTask = require("./Model/TodoTaskSchema");
const bodyParser = require('body-parser');

 const app = express();
 app.use(bodyParser.urlencoded({ extended:true }));
 app.use(bodyParser.json()); // using body parser to parsing incoming request to json

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*'); // Essential for our server to serve cross domain requests;
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const mongoose = require('mongoose');
//connecting to mongo db database connection
mongoose.set("useFindAndModify", false);
mongoose.connect('mongodb://127.0.0.1:27017/todo', {useNewUrlParser: true}, (err) => {
    console.log("Connected to the database and Error - ", err);
    app.listen(3004, (...data) => console.log("Server Up and running", data));
});

//route to handle user login

app.post('/login', async(req, res) => {
    let flag_p = false;
    console.log(req.body);

    await User.find({email: req.body.email}, (err, users) => {
        console.log('Error - ', err);

        try {
            if ((users[0].email) != (undefined)) {
                if (users[0].password === req.body.password) {
                    flag_p = true;
                    console.log('User Successfully Logged In');
                    res.send({
                        isLogin: flag_p,
                        user_details: users,
                    });

                    flag_p = false;
                    console.log(users);
                } else {
                    console.log(" Password didn't matched ");
                    res.send({code: 404, message: 'Wrong Credentials Entered'});
                }
            }
            else {
                console.log('Email didn\'t found');
                res.send({code:406, message: 'Email Doesn\'t exist in record'})
            }
        }
        catch (e) {
            console.log(e);
            res.send({code:406, message: 'Email Doesn\'t exist in record'})
        }
    });
});

//route to handle user registration

app.post('/register', async (req, res) => {
let today = new Date();
//console.log('Request pahoch rhi  hai yaha pr');
//console.log(req);

    const users = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phoneNumber: req.body.phoneNumber,
        password: req.body.password,
        created: today,
        modified: today
    });

try
{
   await users.save();
   res.send({value: users, code: 200, message: "user registered successfully"});
}
catch (err) {
   console.log("Entered in this Error Block")
   if (err) {
       console.log("Error Occured", err);
       res.send({
           code: 400,
           message: 'Error Occured (\'User Already Registered with same email or phone number\')',
           error: err
       });
   }
}
});

// Fetching Tasks

app.get('/usertasks', async (req, res) => {
    console.log("Entered on get call");
    console.log(req);
    await TodoTask.find({userId : req.query.userId}, (err,tasks) => {
        res.send({todoTasks: tasks});
    });
    //console.log(res);
});

// Adding todo_tasks

app.post('/usertasks',async(req, res) => {
    const todoTask = new TodoTask({
        content: req.body.task,
        userId: req.body.userId
    });
    console.log(req.body.userId);
    try {
        await todoTask.save();
    }
    catch(err){
        console.log(err);
    }
    TodoTask.find({userId : req.body.userId}, (err,tasks) => {
        res.send({todoTasks: tasks});
        //console.log(tasks);
    });
});

//DELETE the tasks

app.delete('/remove', async (req, res) => {

    const id = req.body.d_id;
    await TodoTask.findByIdAndRemove(id, err => { console.log(err);
    });
    await TodoTask.find({userId: req.body.userId}, (err,tasks) => {
        res.send({todoTasks: tasks});
    });
});

// UPDATE the tasks
app.put('/update', async(req, res) => {
        const id = req.body.d_id;
        console.log(req.body);
        await TodoTask.findByIdAndUpdate(id, {content: req.body.content}, err => {
            if (err) return res.send(500, err); });
        await TodoTask.find({userId: req.body.userId}, (err, tasks) => {
            res.send({todoTasks: tasks});
        });
    });
